"use strict"; 

    /*Bar chart start*/
    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
                        .x(function(d) { return d.label })    //Specify the data accessors.
                        .y(function(d) { return d.value })
                        .staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
                        /* .tooltips(false)    */    //Don't show tooltips
                        .showValues(true)       //...instead, show the bar value right on top of each bar.
                /*     .transitionDuration(350)*/
                ;

        d3.select('#barchart').append('svg')
                .datum(barData())
                .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
    });

    //Each bar represents a single discrete quantity.
    function barData() {
        return  [
            {
                key: "Cumulative Return",
                values: [
                    {
                        "label" : "Std 8" ,
                        "value" : 10,
                        "color" : "#2196F3"
                    } ,
                    {
                        "label" : "Std 9" ,
                        "value" : 5,
                        "color" : "#ff5252"
                    } ,
                    {
                        "label" : "Std 10" ,
                        "value" : 30,
                        "color" : "#4CAF50"
                    } ,
                    {
                        "label" : "Corner 8" ,
                        "value" : 123,
                        "color" : "#f57c00"
                    } ,
                    {
                        "label" : "Corner 9" ,
                        "value" : 24,
                        "color" : "#FF0084"
                    } ,
                    {
                        "label" : "Corner 10" ,
                        "value" : 2,
                        "color" : "#007BB6"
                    }
                ]
            }
        ]

    }

 

    /*Regular Pie chart*/
    nv.addGraph(function() {
        var chart = nv.models.pieChart()
                .x(function(d) { return d.label })
                .y(function(d) { return d.value })
                .showLabels(true);

        d3.select("#piechart").append('svg')
                .datum(pieData())
                .transition().duration(350)
                .call(chart);
  nv.utils.windowResize(chart.update);
        return chart;
    });
    //Donut chart example
    nv.addGraph(function() {
        var chart = nv.models.pieChart()
                        .x(function(d) { return d.label })
                        .y(function(d) { return d.value })
                        .showLabels(true)     //Display pie labels
                        .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
                        .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
                        .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
                        .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
                ;

        d3.select("#donutchart").append('svg')
                .datum(pieData())
                .transition().duration(350)
                .call(chart);
  nv.utils.windowResize(chart.update);
        return chart;
    });
    //Pie chart example data. Note how there is only a single array of key-value pairs.
    function pieData() {
        return  [
            {
                "label": "Canvasing",
                "value" : 29.765957771107,
                        "color" : "#2196F3"
            } ,
            {
                "label": "Event",
                "value" : 0,
                        "color" : "#ff5252"
            } ,
            {
                "label": "Exhibition",
                "value" : 32.807804682612,
                        "color" : "#4CAF50"
            } ,
            {
                "label": "Media cetak",
                "value" : 196.45946739256,
                        "color" : "#f57c00"
            } ,
            {
                "label": "Media digital",
                "value" : 0.19434030906893,
                        "color" : "#FF0084"
            } ,
            {
                "label": "Media outdoor",
                "value" : 98.079782601442,
                        "color" : "#007BB6"
            } ,
            {
                "label": "Reference",
                "value" : 13.925743130903,
                        "color" : "#3b5998"
            } ,
            {
                "label": "SMS blast",
                "value" : 5.1387322875705,
                        "color" : "#CB2027"
            }
        ];
    }

